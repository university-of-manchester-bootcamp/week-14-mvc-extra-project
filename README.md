# Blogs

## Model
    - title
    - contents
    - who wrote it
    - created on
    - last updated on

## Controller
    - Adding a new blog
    - Getting all existing blogs
    - Updating a blog
    - Getting a single blog
    - Delete a blog (optional)

## Views
    - Show individual blog
    - Show list of all blogs
    - Add and edit blog
    - Delete blog (optional)

---

